/********************************************************************
 * COPYRIGHT -- Cerna Hora
 ********************************************************************
 * Program: RESTClient
 * File: RESTClient.c
 * Author: kre2
 * Created: January 15, 2018
 ********************************************************************
 * Implementation of program RESTClient
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include "string.h"

#include <ashttp.h>

#define REST_MAX_LEN        524280 

_LOCAL BOOL            run;

_LOCAL USINT cliResData[REST_MAX_LEN]; 

_LOCAL USINT host[128];
_LOCAL UINT  port;
_LOCAL USINT url[128];

_LOCAL UDINT retr; 
_LOCAL UDINT retu;
_LOCAL UDINT cnt, err;

typedef struct HTTPData
{
    unsigned long size;
    unsigned long pData;
    plcstring rawHeader[1025];
} HTTPData;

typedef struct HTTPExchange
{	
    unsigned short responseStatusHttp;
    unsigned short responseStatusTcp;
    unsigned short status;
    plcstring requestURI[257];
    struct httpRequestHeader_t requestHTTPHeader;
    struct HTTPData request;
    struct httpResponseHeader_t responseHTTPHeader;
    struct HTTPData response;
    unsigned long maxResponseLen;
    unsigned short method;
} HTTPExchange;

typedef struct HTTPClient
{	
    struct httpClient client;
    struct httpStatistics_t statistics;
    unsigned short state;
    plcstring pHost[65];
    unsigned short port;
    plcstring pUrl[65];
    struct HTTPExchange exchange;
    unsigned long cnt;
    unsigned long sendTime;
} HTTPClient;

_LOCAL HTTPClient owner;
   
unsigned long HTTPClientRequest(struct HTTPClient* pOwner, unsigned long method, unsigned long option, plcstring* pHost, unsigned short port, plcstring* pUrl, plcstring* pReqData, unsigned long reqLen, plcstring* pResData, unsigned long maxResLen, unsigned long timeout)
{ 
    UINT ret = 0;
    pOwner->client.enable = 1;
    pOwner->client.pStatistics = (UDINT) &pOwner->statistics;
    pOwner->client.pRequestHeader = (UDINT) &pOwner->exchange.requestHTTPHeader;
    pOwner->client.pResponseHeader = (UDINT) &pOwner->exchange.responseHTTPHeader;
    switch(pOwner->state)
    {
        default:
        case 0:
            // init
            
//            if(((UDINT)clock_ms() - pOwner->sendTime) >= timeout)
//            { // uplynul cas         
//            }
//            else 
//            { // jeste neuplynul cas
//                break;
//            }
            
            pOwner->client.method = method;
            pOwner->client.option = option;
            
            pOwner->client.pRequestData = pOwner->exchange.request.pData = (UDINT) pReqData;
            pOwner->client.requestDataLen = pOwner->exchange.request.size = reqLen;
            pOwner->client.pResponseData = pOwner->exchange.response.pData = (UDINT) pResData;
            pOwner->client.responseDataSize = maxResLen;
            
            strncpy(pOwner->pHost, pHost, sizeof(pOwner->pHost));
            pOwner->port = port;
            strncpy(pOwner->pUrl, pUrl, sizeof(pOwner->pUrl));           
            pOwner->client.pHost = (UDINT) &pOwner->pHost;
            pOwner->client.hostPort = pOwner->port;
            pOwner->client.pUri = (UDINT) &pOwner->pUrl;
        
            pOwner->client.send = 1;
            pOwner->client.abort = 0;
            pOwner->sendTime = (UDINT)clock_ms();
            ret = 65535;
            pOwner->state = 1;
            break;
        case 1: 
                        
            if(((UDINT)clock_ms() - pOwner->sendTime) >= timeout)
            { // uplynul cas
                pOwner->client.send = 0;
                pOwner->client.abort = 1;            
            }
            else
            { // jeste ceka
                pOwner->client.send = 1;
                pOwner->client.abort = 0;
            }
            pOwner->cnt++; 
            if(pOwner->client.status != 65535)
            { // uz to proslo
                if((pOwner->client.status == 0) && (pOwner->client.httpStatus == 0) && (pOwner->client.tcpStatus == 0))
                {        
                    err++;
                    return pOwner->client.status;
                }
                cnt++;
                pOwner->client.send = 0;
                pOwner->state = 0;
            }
            ret = pOwner->client.status;
            break;
    }
    return ret;
}

_BUR_PUBLIC unsigned long HTTPClientUpdate(struct HTTPClient* pOwner)
{
    httpClient(&pOwner->client);
    return pOwner->client.status;
}


void _INIT RESTClientInit(void)
{
//    strcpy((STRING *)host,"www.seznam.cz");
	strcpy((STRING *)host,"10.42.10.156");
	
    port = 80;
//    strcpy((STRING *)url, "");
    strcpy((STRING *)url, "/sdm/index.html");
}

void _CYCLIC RESTClientCyclic(void)
{
    if(run)
    {   
        retr = HTTPClientRequest(&owner, httpMETHOD_GET, httpOPTION_HTTP_11, (STRING *)host, port, (STRING *)url, 0, 0, (STRING *)cliResData, REST_MAX_LEN, 5000);   
        retu = HTTPClientUpdate(&owner);
    }
}

void _EXIT RESTClientExit(void)
{
}
