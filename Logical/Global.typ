(********************************************************************
 * COPYRIGHT --  
 ********************************************************************
 * File: Global.typ
 * Author: kre2
 * Created: September 21, 2016
 ********************************************************************
 * Global data types of project BaRCore
 ********************************************************************)
(*Tabulky*)

TYPE
	DataPositions : 	STRUCT 
		ID : UDINT; (*Unique code of nest that go one the other
ID = 0 is not valid.*)
		DISABLED : BOOL; (*Is the nest deactivated? (e.g.: due to failures)  0 - No 1 - Yes 	*)
		PERSON_ID : UDINT; (*ID of person, to which the nest is assigned (short time or permanently) 
PERSON_ID = 0 is not valid.*)
		PERMANENT_PERSON : BOOL; (*Is the nest assigned permanently?
0 - No
1 - Yes*)
		ELEMENT_GROUP_ID : UDINT; (*Identifier of a group of subjects (ref. to ID table DataElementGroup)
ELEMENT_GROUP_ID = 0 is not valid.*)
		NEXT_ELEMENT_ID : UDINT; (*Identification of subject (ref. to ID table DataElement)
ELEMENT_ID = 0 is not valid.
This id is copyed to the ELEMENT_ID in Filling process *)
		ELEMENT_ID : UDINT; (*Identification of subject (ref. to ID table DataElement)
ELEMENT_ID = 0 is not valid.*)
	END_STRUCT;
	DataElement : 	STRUCT 
		ID : UDINT; (*Unique element code
ID = 0 is not valid.*) (*Musi byt prvni prvek v struktu�e*)
		STRING_ID : STRING[32]; (*Subject identification code of an identifiable subject. It must be unique for the given type of subject
The content depends upon the given reader type. 
STRING_ID = �� is not valid.*)
		EXPIRATION : DATE_AND_TIME; (*Date, until which the subject may be used.
Date is UTC time of the format according to ISO 8601:
�{YYYY}-{MM}-{DD} {HH}:{MM}:{SS}Z�.*)
		IS_EXPIRED : BOOL; (*The administrator sets the flag of logical prohibiting to use a subject:
0- May be used
1- Must not*)
		IS_USED : BOOL; (*Flag whether a subject was used in DPD since the last filling of DPD:
0- Not used
1- Used*)
	END_STRUCT;
	DataElementGroup : 	STRUCT 
		ID : UDINT; (*Unique code of a subject group ID = 0 is not valid.	*) (*Musi byt prvni prvek v struktu�e*)
		LABEL : WSTRING[32]; (*Subject group description (entered by user), e.g.:
-	FD - film dosimeter
-	ED - electronic dosimeter
-	PD - ring dosimeter
-	TLD-N - neutron TLD dosimeter
KEY - key*)
		INTEGRAL : BOOL; (*Flag whether the subject is assigned to a person for the period of campaign (e.g. FD measures 1 month integrally)
0-	No
1-	Yes
The campaign ends with exchange of subject in the nest.*)
		IDENTIFIED : BOOL; (*Flag, whether a subject is identifiable
0-No
1-Yes*)
		POSITION_START : UINT; (*Number of nest, from which subjects of the group are put into DPD
In the range 1...POSITION_END*)
		POSITION_END : UINT; (*Number of nest, up to which subjects of the group are put into DPD
In POSITION_START range...Total number of nests*)
	END_STRUCT;
	DataPerson : 	STRUCT 
		ID : UDINT; (*Unique code of a person
ID = 0 is not valid.*) (*Musi byt prvni prvek v struktu�e*)
		NAME : WSTRING[64]; (*Full name of person*)
		STRING_ID : STRING[32]; (*Identification code of person inscribed on ID card (it is scanned by the reader of DPD)
The content depends upon the given reader type. 
STRING_ID = �� is not valid.*)
		ALLOWED : DATE_AND_TIME; (*Date, until which the person may enter the RCA
Date is UTC time of the format according to ISO 8601:
�{YYYY}-{MM}-{DD} {HH}:{MM}:{SS}Z�.*)
		PERSON_GROUP_ID : ARRAY[0..3]OF UDINT; (*Identifier of the group, to which the member of staff belongs (ref. to ID table DataPersonGroup). It means the member of staff can get up to 4 types of subjects
ID = 0 is not valid.*)
		CHANGE_INFORMATION : BOOL; (*Flag, whether the person is to be informed about an exchange of subject at the end of campaign 
0- not to inform 
1- to inform (it is set to 0 after being displayed) *)
	END_STRUCT;
	DataPersonGroup : 	STRUCT 
		ID : UDINT; (*Unique code of a group
ID = 0 is not valid.*) (*Musi byt prvni prvek v struktu�e*)
		LABEL : WSTRING[32]; (*Group name*)
		ALLOWED : DATE_AND_TIME; (*Date, until which persons of the group may enter the RCA.
Date is UTC time of the format according to ISO 8601:
�{YYYY}-{MM}-{DD} {HH}:{MM}:{SS}Z�.*)
		ELEMENT_CNT : UDINT; (*Number of subjects from the given subject group that will be dispensed to the person in the range of: 1-4*)
		ELEMENT_GROUP_ID : UDINT; (*Identifier of a group of subjects (ref. to ID table DataElementGroup)*)
	END_STRUCT;
	DataMessages : 	STRUCT 
		ID : UDINT; (*Unique message code
ID = 0 is not valid.*) (*Musi byt prvni prvek v struktu�e*)
		COLOR : UDINT; (*Text colour
It is possible to set one of the following IDs. The background colour is set according to RGB scheme thanks to this.
	Background (component) colour
ID            R              G             B
-------------------------------------
1              0              170          0
2              0              170          255
3              255          255          0
4              255          0              51
5              0              51            204
6              153          102          254
7              255          153          255
8              153          153         153
9              255          136          0
10            255          255          255
11            51            204          102
12            153          204          255*)
		PRIORITY : UDINT; (*0 - informative
1 - warning
Messages of type 1 are classified as actions and sent back to the server.*)
		TEXT : WSTRING[128]; (*Text of message*)
	END_STRUCT;
	DataEvents : 	STRUCT 
		ID : UDINT; (*Code of message (counter) Y set according to relation:
Y = X*100 + DPD_ID
Where X is a sequence of numbers begins from 1. The DPD begins from 1 again in case of overflow (Y value = 42949672xx).
Always filled in*) (*Musi byt prvni prvek v struktu�e*)
		EVENT_TIME : DATE_AND_TIME; (*Date and time when the event occurred
Date is UTC time of the format according to ISO 8601:
�{YYYY}-{MM}-{DD} {HH}:{MM}:{SS}Z�.
Always filled in*)
		EVENT_TYPE : UDINT; (*Type of event (ref. to ID table EventsCode) 
Always filled in*)
		ADMIN_LVL : USINT; (*Access level to DPD 
0 - Member of staff / General event
1 - Operator - action from menu
2 - Administrator - action from menu
3 - Service - action from menu
Always filled in*)
		DPD_ID : USINT; (*DPD Master identification*)
		POSITION_ID : UDINT; (*Identification of nest (ref. to ID table DataPosition) 
0 = left blank
Optional*)
		PERSON_ID : UDINT; (*Identifier of the person that handled with the subject (ref. to ID table DataPerson) 
0 = left blank
Optional*)
		ELEMENT_ID : UDINT; (*Identification of subject that was handled (ref. to ID table DataElement) 
0 = left blank
Optional*)
		PARAMETER : UDINT; (*General parameter - a table of events is provided, where appropriate.
0 = left blank
Optional*)
	END_STRUCT;
END_TYPE

(*Konfigurace*)

TYPE
	ConfigUser : 	STRUCT 
		PASSWORD : STRING[32];
		USER : STRING[32];
	END_STRUCT;
	ConfigCommHostUDP : 	STRUCT 
		SERVER_IP : STRING[64];
		PORT : UINT;
	END_STRUCT;
	ConfigCommHost : 	STRUCT 
		HOST : STRING[64];
		PATH : STRING[32];
		PORT : UINT;
	END_STRUCT;
	ConfigCommunication : 	STRUCT 
		SERVER : ConfigCommHostUDP;
		IM_CLIENT : BOOL;
		USER : ConfigUser;
	END_STRUCT;
	ConfigContamination : 	STRUCT 
		ENABLED : BOOL;
		CALIBRATION_CONSTANT : REAL;
		SIGNAL_LVL : REAL;
	END_STRUCT;
	ConfigBase : 	STRUCT 
		ID : USINT;
		LABAEL : WSTRING[16];
		AUTO_LOGOUT_INTERVAL : UDINT; (*[ms]*)
		USE_HOOTER : BOOL;
		MESSAGE_DISP_ITERVAL : UDINT; (*[ms]*)
		ACTIVE : BOOL;
		LANGUAGE : UINT;
	END_STRUCT;
	ConfigNests : 	STRUCT 
		CASSETTES_TOTAL : USINT;
		CASSETTES_IN_BOX : USINT;
	END_STRUCT;
	ConfigSIKFormat : 	STRUCT 
		ENABLE : BOOL;
		GROUP_OFFSET : USINT;
		CONVER_TO_DEC : USINT;
		DELETE_CHARS : USINT;
	END_STRUCT;
	ConfigSIKSettings : 	STRUCT 
		ENABLED : USINT;
		FORMAT : ConfigSIKFormat;
		MODE : STRING[64];
	END_STRUCT;
	ConfigSIK : 	STRUCT 
		SIE : ConfigSIKSettings;
		SIK : ConfigSIKSettings;
	END_STRUCT;
	Configuration : 	STRUCT 
		COMMUNICATION : ConfigCommunication;
		CONTAMINATION : ConfigContamination;
		BASE : ConfigBase;
		NESTS : ConfigNests;
		IK : ConfigSIK;
	END_STRUCT;
END_TYPE
