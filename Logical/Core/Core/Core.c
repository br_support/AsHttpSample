/********************************************************************
 * COPYRIGHT -- Cerna Hora
 ********************************************************************
 * Program: Core
 * File: Core.c
 * Author: kre2
 * Created: July 11, 2017
 ********************************************************************
 * Implementation of program Core
 ********************************************************************/

#include <bur/plctypes.h>

#ifdef _DEFAULT_INCLUDES
	#include <AsDefault.h>
#endif

#include <string.h>

#include "Global.h"

#include "VFArLog.h"
#include "VFTime.h"
#include "VFConvert.h"
#include "VFXml.h"
#include "VFCfgEth.h"
#include "VFRemData.h"
#include "VFBuffSrv.h"
#include "CoreCheckConfig.h"
 
//#include "Events/Events.h"

// Globalne definovana struktura AssemblyInfo
//const AssemblyInfo assemblyInfo;
#include "../../../AssemblyInfo.h"

// Obecne parametry 
_LOCAL USINT                first;
_LOCAL CfgGetMacAddr_typ    GetMAC;
_LOCAL USINT                MAC[6];
_LOCAL STRING               stringMAC[24];
_LOCAL VFDir                dir;
_LOCAL VFTime               time;
_LOCAL DTStructure          currentTime;
_LOCAL STRING               timeStamp[32];

// Globalni parametry
_LOCAL VFDeviceLink         devLink[4];

typedef struct CoreConfigDriver
{
    VFCoreCfg               cfg;            // Ridi nacitani souboru ze souboru
    VFCoreCfgSetup          configSetup;    // Ridi nastavovani parametru v PLC
    VFCoreCfgExport         cfgExport;      // Ridi export dat do souboru
}CoreConfigDriver;


//=============================================================================
// Zakladni konfiguracni soubor

_LOCAL  CoreConfigDriver    coreConfigDriver;

_GLOBAL VFCoreCfgStruct     coreConfiguration;  // Struktura obsahujici nactene parametry z konfigurace

// Definice obsahu konfiguracniho souboru
VFXmlElementArray coreConfigParams[] = {
    // Nazev promene                Nazev elementu v konfig souboru
    {"coreConfiguration",           "basic"}
};

// Udalost nacteni konfiguracniho souboru
UDINT OnRecvCoreConfig(USINT * pData, UDINT len)
{
    UDINT result = 0;
    result = VFXmlArrayDeserializer(coreConfigParams, sizeof(coreConfigParams)/sizeof(VFXmlElementArray), "configuration", pData, len, 0);
    
    if(result == 0)
    { // Nacteni konfigurace probeho vporadku
        if ( (first > 0) )// && coreConfiguration.force)
        { // pokud nacteni po startu tak se nacte i appconfig
            configControl[1].cmd.loadSearch = 1;
            
            if (coreConfiguration.force)
                configControl[0].param.doAfterLoad = 1;
            else
                configControl[0].param.doAfterLoad = 0;
            first = 0;
        }
        else
            configControl[0].param.doAfterLoad = 1;
               
        VFCoreCfgSetupInit(&coreConfigDriver.configSetup, &coreConfiguration); // s return nic nenadelam
    }
    return 0;
}

// Udalost exportu konfiguracniho souboru
UDINT OnExportCoreConfig(USINT * pData, UDINT * len)
{
    UDINT result = 0;
    result = VFCoreCfgGetUpdate(&coreConfigDriver.configSetup, &coreConfiguration, ETH_INTERFACE_A, ETH_INTERFACE_B, 2);
    if(result != 65535)
    { // dobeho nacitani parametru
        VFConvertSprintf2(coreConfigDriver.cfgExport.param.fileName , "%s_%s_internal.xml", (UDINT)appAssemblyInfo.assembly, (UDINT)stringMAC); // konkretni nazev konfiguracniho souboru
        result = VFXmlArraySerializer(coreConfigParams, sizeof(coreConfigParams)/sizeof(VFXmlElementArray), "configuration", pData, len, 0, 0);
    }
    return result;
}


// PRO Pou�it� aplika�n� konfigurace sta�� toto odkomentovat
//=============================================================================
// Aplikacni konfigurace
#define USE_APP_CONFIG

#define APP_CONFIG_NAME "AppConfig"

_LOCAL  CoreConfigDriver    appConfigDriver;
_LOCAL  VFRemData           appCfgBackup;

_GLOBAL Configuration       appConfigImport;

_LOCAL  UDINT               appConfigMagic;

// Definice obsahu konfiguracniho souboru
VFXmlElementArray appConfigParamsImport[] = {
    // Nazev promene                Nazev elementu v konfig souboru
    {"appConfigImport",           "dpdConfiguration"}
    };

// Definice obsahu konfiguracniho souboru
VFXmlElementArray appConfigParamsExport[] = {
    // Nazev promene                Nazev elementu v konfig souboru
    {"config",           "dpdConfiguration"}
    };

// Udalost nacteni konfiguracniho souboru
UDINT OnRecvAppConfig(USINT * pData, UDINT len)
{
    UDINT result = 0;
    result = VFXmlArrayDeserializer(appConfigParamsImport, sizeof(appConfigParamsImport)/sizeof(VFXmlElementArray), APP_CONFIG_NAME, pData, len, 0);
    if(result == 0)
    { // Nacteni konfigurace probeho vporadku
        if(AppConfigValid(&appConfigImport, APP_CONFIG_MAGIC))
        {
            memcpy(&config, &appConfigImport, sizeof(config));
            appConfigMagic = APP_CONFIG_MAGIC;
            configControl[1].info.configResultSetup = 0;
        }
        else
        {
            configControl[1].info.configResultSetup = 1;	
        } 
    }
    return 0;
}

// Udalost exportu konfiguracniho souboru
UDINT OnExportAppConfig(USINT * pData, UDINT * len)
{
    UDINT result = 0;
    VFConvertSprintf2(appConfigDriver.cfgExport.param.fileName , "%s_%s_config.xml", (UDINT)appAssemblyInfo.assembly, (UDINT)stringMAC); // konkretni nazev konfiguracniho souboru
    result = VFXmlArraySerializer(appConfigParamsExport, sizeof(appConfigParamsExport)/sizeof(VFXmlElementArray), APP_CONFIG_NAME, pData, len, 0, 0);
    return result;
}

void ConfigControlUpdate(CoreConfigDriver * pOwner, VFCommonKonfig * pControl, STRING * timeStamp, UDINT onExport)
{
    pControl->info.usedConfig = 1;
    strcpy(pControl->param.exportName, pOwner->cfg.param.primaryFileName); 
    
    if(pControl->cmd.loadSearch)
    { // Ma se nacist konfiguracni soubor      
        
        // Samotne nacitani konfigurace    
        pControl->info.configResultRead = VFCoreCfgUpdate(&pOwner->cfg, timeStamp);
        if (pControl->info.configResultRead != 65535)
            pControl->cmd.loadSearch = 0;
    }
 
    if(pControl->cmd.export)
    { // Ma se exportovat konfiguracni soubor
        pControl->info.configResultExport = VFCoreCfgExportUpdate(&pOwner->cfgExport, pControl->param.exportDevice, pControl->param.exportName, onExport);
        if (pControl->info.configResultExport != 65535)
            pControl->cmd.export = 0;
    }
}

void _INIT CoreInit(void)
{
    STRING defName[64];
    memcpy(&appAssemblyInfo, &assemblyInfo, sizeof(appAssemblyInfo));
       
    // Nalinkovani zarizeni
    VFDevLink(&devLink[0], (UDINT) deviceC, (UDINT) "C:\\", 0);
    VFDevLink(&devLink[1], (UDINT) deviceF, (UDINT) "F:\\", 0);
    VFDevLink(&devLink[2], (UDINT) deviceUSB1, (UDINT) "/bd0", 0);
    VFDevLink(&devLink[3], (UDINT) deviceBackup, (UDINT) "F:\\OldConfigs\\", 0);
    // MAC adresa PLC
    VFCfgEthGetMac(&GetMAC, ETH_INTERFACE_A, (UDINT) MAC, 6);
    VFCfgEthMac(stringMAC, MAC);

    // Vytvori po restartu slozku F:\oldConfigs\ pokud neexistuje 
    while(VFDirCreate(&dir, (UDINT) &deviceF, (UDINT) &oldConfigs) == 65535){};
    
    VFConvertSprintf2(defName, "%s_%s_internal.xml", (UDINT)appAssemblyInfo.assembly, (UDINT)stringMAC); // konkretni nazev konfiguracniho souboru
    VFCoreCfgInit(&coreConfigDriver.cfg, defName, "internal.xml", (STRING *)deviceUSB1, (STRING *)deviceC, (STRING *)deviceBackup, (UDINT)OnRecvCoreConfig);
    
#ifdef USE_APP_CONFIG  
    VFConvertSprintf2(defName, "%s_%s_config.xml", (UDINT)appAssemblyInfo.assembly, (UDINT)stringMAC); // konkretni nazev konfiguracniho souboru
    VFCoreCfgInit(&appConfigDriver.cfg, defName, "config.xml", (STRING *)deviceUSB1, (STRING *)deviceC, (STRING *)deviceBackup, (UDINT)OnRecvAppConfig);
    // Automatick� z�loha parametru konfigurace
    VFRemDataAdd(&appCfgBackup, (UDINT)&config, sizeof(config)); // je potreba jeste pridat mou strukturu + pokud je pot�eba tak ji jeste nejak kontrolovat...
    VFRemDataAdd(&appCfgBackup, (UDINT)&appConfigMagic, sizeof(appConfigMagic));
    VFRemDataInit(&appCfgBackup, "ccBa");
#endif
    
    // Nacteni konfigurace po startu
    first = 32;
    configControl[0].cmd.loadSearch = 1;
    
    // Init Logu
    while(VFArLogInit(&coreLog, "VFLog", 262144, arlogUSRROM) == ERR_FUB_BUSY){}    
}

void _CYCLIC CoreCyclic(void)
{
    // Nacteni konfigurace po startu
    if(first > 0)
        first--;
    
#ifndef USE_APP_CONFIG
    configControl[1].info.usedConfig = 0;
#endif
    
    AppConfigHoldCorrect(&config);
    dpdStatuses.isConfigOK = AppConfigValid(&config, appConfigMagic);
    // Casova znacka
    VFTimeGetLocalTimeDtStructure(&time, &currentTime);        
    VFConvertSprintf6(timeStamp, "%04u%02b%02b%02b%02b%02b", (UDINT)&currentTime.year, (UDINT)&currentTime.month, (UDINT)&currentTime.day, (UDINT)&currentTime.hour, (UDINT)&currentTime.minute, (UDINT)&currentTime.second);

    //=============================================================================
    // Zakladni konfiguracni soubor
    ConfigControlUpdate(&coreConfigDriver, &configControl[0], timeStamp, (UDINT) OnExportCoreConfig);
    
    if(configControl[0].param.doAfterLoad)
    {
        configControl[0].info.configResultSetup = VFCoreCfgSetupUpdate(&coreConfigDriver.configSetup);
        if (configControl[0].info.configResultSetup != 65535)
        configControl[0].param.doAfterLoad = 0;
    }
    
#ifdef USE_APP_CONFIG    
    //=============================================================================
    // Aplikacni konfigurace    
    ConfigControlUpdate(&appConfigDriver, &configControl[1], timeStamp, (UDINT) OnExportAppConfig);
    
    VFRemDataUpdate(&appCfgBackup);
#endif
    
    // Update Logu
    VFArLogUpdate(&coreLog);
}

void _EXIT CoreExit(void)
{
#ifdef USE_APP_CONFIG
    // Uvolneni prost�edk�
    VFRemDataExit(&appCfgBackup);
#endif
}
