/********************************************************************
 * COPYRIGHT -- Cerna Hora
 ********************************************************************
 * Program: Core
 * File: CoreCheckConfig.h
 * Author: kre2
 * Created: October 14, 2017
 *******************************************************************/

#ifndef __CORE_CHECK_CONFIG_H__
#define __CORE_CHECK_CONFIG_H__

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#define APP_CONFIG_MAGIC    157953

// Vrati zda je dana konfigurace v poradku
BOOL AppConfigValid(Configuration * pConfig, UDINT magic);

// udrzuje hodnoty v konfiguraci v mezich
void AppConfigHoldCorrect(Configuration * pConfig);

#endif //__CORE_CHECK_CONFIG_H__
