﻿<?xml version="1.0" encoding="utf-8"?>
<?AutomationStudio Version=4.1.11.118 SP?>
<Program xmlns="http://br-automation.co.at/AS/Program">
  <Files>
    <File Description="Implementation code">Core.c</File>
    <File Description="Local data types" Private="true">Core.typ</File>
    <File Description="Local variables" Private="true">Core.var</File>
    <File>CoreCheckConfig.c</File>
    <File>CoreCheckConfig.h</File>
  </Files>
</Program>