/********************************************************************
 * COPYRIGHT -- Cerna Hora
 ********************************************************************
 * Program: Core
 * File: CoreCheckConfig.c
 * Author: kre2
 * Created: October 14, 2017
 *******************************************************************/

#include <bur/plctypes.h>
#ifdef _DEFAULT_INCLUDES
#include <AsDefault.h>
#endif

#include <string.h>

#include "Global.h"

#include "CoreCheckConfig.h"

// Vrati zda je dana konfigurace v poradku
BOOL AppConfigValid(Configuration * pConfig, UDINT magic)
{
    if(magic != APP_CONFIG_MAGIC)return 0;
    // Base
    if(!pConfig->BASE.ID) return 0;
    if(pConfig->BASE.ID > 99) return 0;
    //if(!pConfig->BASE.AUTO_LOGOUT_INTERVAL) return 0;
    if(!pConfig->BASE.MESSAGE_DISP_ITERVAL) return 0;
  
    // communication
    if(!pConfig->COMMUNICATION.SERVER.PORT) return 0; 

    // kontaminace
    if(pConfig->CONTAMINATION.CALIBRATION_CONSTANT <= 0.0) return 0;
    if(pConfig->CONTAMINATION.SIGNAL_LVL <= 0.0) return 0;
    
    // Parametry hnizd
    if(pConfig->NESTS.CASSETTES_IN_BOX == 0) return 0;
    if(pConfig->NESTS.CASSETTES_IN_BOX > 6) return 0;
    if(pConfig->NESTS.CASSETTES_TOTAL == 0) return 0;
    if((pConfig->NESTS.CASSETTES_TOTAL * DPD_NESTS_IN_ROW) > DATA_POSITIONS_CNT) return 0;
    return 1;
}

// udrzuje hodnoty v konfiguraci v mezich
void AppConfigHoldCorrect(Configuration * pConfig)
{
    // Base
    pConfig->BASE.ID = (pConfig->BASE.ID > 99)?99:pConfig->BASE.ID;
    //pConfig->BASE.AUTO_LOGOUT_INTERVAL = (pConfig->BASE.AUTO_LOGOUT_INTERVAL == 0)?1:pConfig->BASE.AUTO_LOGOUT_INTERVAL;
    pConfig->BASE.ACTIVE = (pConfig->BASE.ACTIVE)?1:0;
    if(pConfig->BASE.LABAEL[0] == 0)
    {
        pConfig->BASE.LABAEL[0] = 'D';pConfig->BASE.LABAEL[0] = 'P';pConfig->BASE.LABAEL[0] = 'D';pConfig->BASE.LABAEL[0] = 0;
    }
    pConfig->BASE.MESSAGE_DISP_ITERVAL = (pConfig->BASE.MESSAGE_DISP_ITERVAL == 0)?1:pConfig->BASE.MESSAGE_DISP_ITERVAL;
    pConfig->BASE.USE_HOOTER = (pConfig->BASE.USE_HOOTER)?1:0;
    
    // communication
    pConfig->COMMUNICATION.IM_CLIENT = (pConfig->COMMUNICATION.IM_CLIENT)?1:0;
    pConfig->COMMUNICATION.USER.PASSWORD[sizeof(pConfig->COMMUNICATION.USER.PASSWORD) - 1] = 0;
    pConfig->COMMUNICATION.USER.USER[sizeof(pConfig->COMMUNICATION.USER.USER) - 1] = 0;
    pConfig->COMMUNICATION.SERVER.PORT = (pConfig->COMMUNICATION.SERVER.PORT == 0)?22086:pConfig->COMMUNICATION.SERVER.PORT;

    // kontaminace
    pConfig->CONTAMINATION.ENABLED = (pConfig->CONTAMINATION.ENABLED)?1:0;
    pConfig->CONTAMINATION.CALIBRATION_CONSTANT = (pConfig->CONTAMINATION.CALIBRATION_CONSTANT <= 0.0)?2.48:pConfig->CONTAMINATION.CALIBRATION_CONSTANT;
    pConfig->CONTAMINATION.SIGNAL_LVL = (pConfig->CONTAMINATION.SIGNAL_LVL <= 0.0)?1.5:pConfig->CONTAMINATION.SIGNAL_LVL;
    
    // �te�ka IK
    pConfig->IK.SIK.ENABLED = (pConfig->IK.SIK.ENABLED)?1:0;
    if(strlen(pConfig->IK.SIK.MODE) == 0) strcpy(pConfig->IK.SIK.MODE, "/PHY=RS232 /PA=N /DB=8 /SB=1 /BD=9600");
    
    // �te�ka IE
    pConfig->IK.SIE.ENABLED = (pConfig->IK.SIE.ENABLED)?1:0;
    if(strlen(pConfig->IK.SIK.MODE) == 0) strcpy(pConfig->IK.SIK.MODE, "/PHY=RS232 /PA=N /DB=8 /SB=1 /BD=9600");
    
    // Parametry hnizd
    pConfig->NESTS.CASSETTES_IN_BOX = (pConfig->NESTS.CASSETTES_IN_BOX == 0)?1:pConfig->NESTS.CASSETTES_IN_BOX;
    pConfig->NESTS.CASSETTES_IN_BOX = (pConfig->NESTS.CASSETTES_IN_BOX > 6)?6:pConfig->NESTS.CASSETTES_IN_BOX;    
    pConfig->NESTS.CASSETTES_TOTAL = (pConfig->NESTS.CASSETTES_TOTAL == 0)?1:pConfig->NESTS.CASSETTES_TOTAL;
    pConfig->NESTS.CASSETTES_TOTAL = ((pConfig->NESTS.CASSETTES_TOTAL * DPD_NESTS_IN_ROW) > DATA_POSITIONS_CNT)?(DATA_POSITIONS_CNT / DPD_NESTS_IN_ROW):pConfig->NESTS.CASSETTES_TOTAL;    
}

